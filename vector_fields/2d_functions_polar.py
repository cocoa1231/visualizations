#!/usr/bin/env python
       
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
from matplotlib.animation import FuncAnimation
from sys import argv
        
fig = plt.figure(figsize=(30, 30))
ax  = plt.gca()
ax.set_xlim(-10, 10)
ax.set_ylim(-10, 10)
       
x, y  = np.linspace(-1,1, 40), np.linspace(-1, 1, 40)
r     = np.linspace(0, 1, 30)
theta = np.linspace(0, 2*np.pi, 30)
       
X, Y  = np.meshgrid(x, y)
       
drops = {}
       
F = lambda r, t : (1/r, t)

for rv in r:
    for av in theta:
        rp, ap = F(rv, av)
        drops[ax.add_patch(Circle( (rv * np.cos(av), rv * np.sin(av)), .1, color=(rv,.5,.5) ))] = [ rp * np.cos(ap), rp * np.sin(ap) ]

def update(frame, dlist : dict):

    if frame > 30:
        for drop, target in dlist.items():
            x, y = drop.center
            u, v = target

            vec = np.array([u - x, v - y])
            dx = vec[0] * 1 / 10 
            dy = vec[1] * 1 / 10 

            drop.center = (x + dx, y + dy)
    return dlist

ani = FuncAnimation(fig, update, frames=10*20, fargs=(drops,), interval=1000/20)
ani.save(argv[1])
