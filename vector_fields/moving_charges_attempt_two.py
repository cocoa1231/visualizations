#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import *
from matplotlib.animation import FuncAnimation
from sys import argv

xlim = 10
ylim = 10
pts  = 30

class ElectricField:
    def __init__(self, charges: dict):
        self.charges = charges
        self.Ex = np.zeros( (pts, pts) ) 
        self.Ey = np.zeros( (pts, pts) )

        
    def field_at_point(self, point):
        for charge, val in self.charges.items():
            posx, posy = val[0]
            poix, poiy = point

            posvec = np.array( (poi-posx, poiy-posx) )
            unit_posvec = posvec / np.hypot(*posvec)
            Emag = charge / np.hypot(*posvec) ** 2
            
        return Emag * unit_posvec[0], Emag * unit_posvec[1]

    def step(self, dt):
        pass
