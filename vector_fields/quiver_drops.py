#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
from matplotlib.animation import FuncAnimation
from sys import argv

fig = plt.figure(figsize=(30, 30))
ax  = plt.gca()
ax.set_xlim(-10, 10)
ax.set_ylim(-10, 10)

colors = ["red", "blue", "green", "yellow", "orange"]

x, y = np.linspace(-10,10, 30), np.linspace(-10, 10, 30)
X, Y = np.meshgrid(x, y)

F = lambda x, y : (y, -2*y - 4/3 * np.sin(x))

U, V = Y, -2*Y - 1/3 * np.sin(X)
Q1 = ax.quiver(X, Y, U, V)

drops      = []
velocities = []

for xv in x:
    for yv in y:
        drops.append(ax.add_patch(Circle((xv, yv), .1)))
        velocities.append([0, 0])

def update(frame, dlist, vlist):
    for drop in dlist:
        d_index = dlist.index(drop)
        x, y = drop.center
        dx, dy = F(x, y)
        vlist[d_index][0] += dx/1000 
        vlist[d_index][1] += dy/1000
        # drop.center = (x + vlist[d_index][0], y + vlist[d_index][1])
        drop.center = (x + dx/100, y + dy/100)
    return dlist 


ani = FuncAnimation(fig, update, frames=10*60, fargs=(drops, velocities,), interval=1000/60)
ani.save(argv[1])
