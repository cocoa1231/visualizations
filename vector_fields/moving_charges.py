#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
from matplotlib.animation import FuncAnimation
from sys import argv

from random import randint

xlim      = 10
ylim      = 10 
precision = 30

fig = plt.figure(figsize=(30, 30))
ax  = plt.gca()
ax.set_xlim(-xlim, xlim)
ax.set_ylim(-ylim, ylim)

#================================================
# Create vector field mathematically

def E(charge: float, pos: tuple, poi: tuple):
    """
    charge     : Charge of the particle
    posx, posy : x, y giving the x and y coords of
                 the position of the charge 
    poix, poiy : Point at which to calculate the E field
    """

    posx, posy = pos
    poix, poiy = poi 
    
    posvec      = np.array((poix - posx, poiy - posy))
    unit_posvec = posvec / np.hypot(*posvec) 
    Emag        = charge / np.hypot(*posvec)**2

    return Emag * unit_posvec[0], Emag * unit_posvec[1]

# I don't think this is needed
def Force(charges: list, t_charge: float, t_locx, t_locy) -> tuple:
    Fx, Fy = .0, .0
    for charge in charges:
        ex, ey = E(*charge, t_locx, t_locy)
        Fx += ex * t_charge
        Fy += ey * t_charge
    return (Fx, Fy)


x,  y  = np.linspace(-xlim, xlim, precision), \
         np.linspace(-ylim, ylim, precision)
xx, yy = np.meshgrid(x, y)

charges     = [ [ randint(-3, 3),                        # charge
                  (randint(-10, 10), randint(-10, 10)),  # position
                  (0, 0) ]                               # velocity
                  for i in range(int(argv[1])) ]
print(charges)
charge_plot = []

Ex, Ey = np.zeros(xx.shape), np.zeros(yy.shape)

for charge in charges:
    ex, ey = E(*charge[:2], (xx, yy))
    Ex += ex
    Ey += ey
    _color = 'red' if charge[0] > 0 else 'blue'
    charge_plot.append(ax.add_patch( Circle( (charge[1][0], charge[1][1]), .3*abs(charge[0]), color=_color) ))

#================================================
# Animate the field



def update(frame: int, clist: list, patch_list: list) -> list:
    Fx, Fy = .0, .0
    for charge in clist:

        c_index = clist.index(charge)
        q = charge[0]
        cx, cy  = charge[1]
        cv_x, cv_y = charge[2]

        effective_charges = clist[:]
        effective_charges.pop(c_index)

        for echarge in effective_charges:
            ex, ey = E(*echarge[:2], (cx, cy))
            Fx, Fy = ex*q, ey*q

        cv_x += Fx/100
        cv_y += Fy/100
        cx += cv_x
        cy += cv_y
        
        charge[1] = (cx, cy)
        charge[2] = (cv_x, cv_y)

        patch_list[c_index].center = charge[1]

    return patch_list



#================================================
# Make the plot

ani = FuncAnimation(fig, update, frames=2*180, fargs=(charges, charge_plot,),
        interval=100/60)

ani.save("electric_field.mp4")

# veclen = np.hypot(Ex, Ey)
# color = 2 * np.log(veclen)
# ax.quiver(xx, yy, Ex/veclen, Ey/veclen, cmap=plt.cm.inferno)

# plt.show()
