#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import axes3d
from matplotlib.colors import Colormap

fig = plt.figure()
ax  = fig.gca(projection='3d')

space = np.linspace(-2, 2, 5)
x, y, z = np.meshgrid(space, space, space) # R3 :P

u, v, w = y*z, x*z, x*y

qv = ax.quiver(x, y, z, u, v, w, normalize=True)
# curl = ax.quiver(x, y, z, z-y, x-z, y-x, color='red', normalize=True)

plt.show()
