#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np

fig = plt.figure()
ax = fig.gca()

x, y = np.meshgrid( np.arange(-1, 1, .05),
                    np.arange(-1, 1, .05) )

u, v = x, x

ax.quiver(x, y, u, v,)
plt.grid()
plt.show()
