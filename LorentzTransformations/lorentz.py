#!/usr/bin/env python

import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
from matplotlib.animation import FuncAnimation
from sys import argv 
import numpy as np

"""
It's not the best, but it sort of works
Bite me 
"""

def get_arg(flag):
    return argv[argv.index(flag) + 1]

fprops = {
        'family' : 'DejaVu Serif',
        'color' : (1,1,1,1),
        'weight' : 'normal',
        'size': 20
        }

# Constants to configure the plot
xlim = 10 
ylim = 10 
target_vel = float(get_arg('-v')) # Fraction of c
duration = float(get_arg('-s')) # Number of frames
filename = get_arg('-f')
framerate = 60 # FPS

# Data for plotting
grid = [ [(-xlim, y), (xlim, y)] for y in np.arange(-ylim, ylim+1, 1) ] \
        + [ [(x, -ylim), (x, ylim)] for x in np.arange(-xlim, xlim+1, 1) ]
colors = [ (1,1,1,0.7) for i in range(len(grid)) ]
y_axis = grid.index( [(0,-ylim), (0,ylim)] )
x_axis = grid.index( [(-xlim, 0), (xlim, 0)] )
colors[x_axis] = (0,0.52,0.68,0.9)
colors[y_axis] = (0,0.52,0.68,0.9)

# Main functions for plotting and animation
c = 1
gamma = lambda vel : 1 / np.sqrt(1 - vel**2 / c ** 2) # Gamma function

def lorentz(point, vel):
    L_vel = np.array([ [1,        vel],
                       [vel / c**2, 1] ]) # for some reason, vel > 0...

    point = np.array(point) * gamma(vel)
    return tuple(L_vel.dot(point))


def update(frame, grid, linecol, quivers : dict, label):

    current_vel = (target_vel / (framerate * duration)) * frame
    newgrid = grid[:]
    for line_index in range(len(grid)):
        line = grid[line_index]
        start_point = line[0]
        end_point = line[1]

        newline = [ lorentz(start_point, current_vel), lorentz(end_point, current_vel) ]
        newgrid[line_index] = newline


    for point, quiver in quivers.items():
        quiver.set_UVC(*lorentz(point, current_vel))

    label.set_text("$v = {}$ \n $f = {}$".format(current_vel, frame))
    linecol.set_segments(newgrid)
    return (linecol,label,)

# Adding objects to plot
lc = LineCollection(grid, colors=colors)
fig = plt.figure(figsize = (15,15))
ax = fig.gca()

theta = np.linspace(0, 2*np.pi)
vectorlist = [ (1/np.cos(t), np.tan(t)) for t in theta ]

textobj = plt.text(-9, 9, r"$v = {}$".format(target_vel), fontdict=fprops)
plt.subplots_adjust(left=0,right=1,top=1,bottom=0)
ax.add_collection(lc)
hypex, hypey = 1 / np.cos(theta), np.tan(theta)

quivers = {}
for vector in vectorlist:
    quivers[vector] = ax.quiver(0, 0, vector[0], vector[1], color = (1,1,1,1), width = 0.002, scale=1,scale_units='xy')


ax.set_xlim((-5,5))
ax.set_ylim((-5,5))
ax.set_facecolor((0,0,0,1))
ax.autoscale()

ani = FuncAnimation(fig, update, frames=int(framerate*duration), fargs=(grid, lc, quivers, textobj,), interval=1000/framerate)
ani.save(filename)
